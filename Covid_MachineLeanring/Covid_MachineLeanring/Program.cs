﻿using Microsoft.ML;
using System;

namespace Covid_MachineLeanring
{
    class Program
    {
        static void Main(string[] args)
        {
            var context = new MLContext();

            //load data
            var trainData = context.Data.LoadFromTextFile<CaseData>("./Covid.csv", hasHeader:true,
                separatorChar: ',');

            var minMaxEstimator = context.Transforms.NormalizeMinMax("Label");
            ITransformer minmaxTransformer = minMaxEstimator.Fit(trainData);
            IDataView transformedData = minmaxTransformer.Transform(trainData);

            var testTrainSplit = context.Data.TrainTestSplit(trainData, testFraction: 0.25);

            //Build Model
            var pipeline = context.Transforms.Concatenate("Features","date","newTests").Append(context.Regression.Trainers.LbfgsPoissonRegression());
            var model = pipeline.Fit(testTrainSplit.TrainSet);

            //Evaluate

            var predictions = model.Transform(testTrainSplit.TestSet);

            var metrics = context.Regression.Evaluate(predictions);

            Console.WriteLine($"R^2 - {metrics.RSquared}");

            Random r = new Random();
            int rInt;
            //Prediction
            for (int i = 0; i < 20; i++)
            {
                var newdata = new CaseData
                {
                    date = 109.00f + i,
                    newTests = r.Next(35000, 65000)
            };


                var predictionFunc = context.Model.CreatePredictionEngine<CaseData, CasePrediction>(model);

                var prediction = predictionFunc.Predict(newdata);

                Console.WriteLine($"Prediction - {prediction.PredictedCases}");
            }

            Console.ReadLine();
           // NewTestsPredictions();
        }

        public static void NewTestsPredictions()
        {
            var context = new MLContext();

            //load data
            var trainData = context.Data.LoadFromTextFile<CaseData>("./Covid.csv", hasHeader: true,
                separatorChar: ',');

            var testTrainSplit = context.Data.TrainTestSplit(trainData, testFraction: 0.25);

            //Build Model
            var pipeline = context.Transforms.Concatenate("Features", "date").Append(context.Regression.Trainers.LbfgsPoissonRegression("newTests"));
            var model = pipeline.Fit(testTrainSplit.TrainSet);

            //Evaluate

            var predictions = model.Transform(testTrainSplit.TestSet);

            var metrics = context.Regression.Evaluate(predictions);

            Console.WriteLine($"R^2 New Tests - {metrics.RSquared}");

            Random r = new Random();
            int rInt;
            //Prediction
            for (int i = 0; i < 10; i++)
            {
                var newdata = new CaseData
                {
                    date = 109.00f + i
                };


                var predictionFunc = context.Model.CreatePredictionEngine<CaseData, CasePrediction>(model);

                var prediction = predictionFunc.Predict(newdata);

                Console.WriteLine($"Prediction - {prediction.PredictedCases}");
            }

            Console.ReadLine();
        }
    }
}
