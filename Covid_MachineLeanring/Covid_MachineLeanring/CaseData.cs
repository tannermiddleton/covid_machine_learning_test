﻿using Microsoft.ML.Data;
using System;

namespace Covid_MachineLeanring
{
    public class CaseData
    {
        [LoadColumn(0)]
        public float date;

        [LoadColumn(7),ColumnName("newTests")]
        public float newTests;

        [LoadColumn(3), ColumnName("cases")]
        public float cases;

        [LoadColumn(4), ColumnName("newDeaths")]
        public float newDeaths;

        [LoadColumn(5),ColumnName("Label")]
        public float caseCount;
    }
}