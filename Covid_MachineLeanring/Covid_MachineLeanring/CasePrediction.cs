﻿using Microsoft.ML.Data;

namespace Covid_MachineLeanring
{
    internal class CasePrediction
    {
        [ColumnName("Score")]
        public float PredictedCases { get; set; }
    }
}